FROM golang:1.9-alpine AS builder

WORKDIR /app

COPY ./main .

ENV GO111MODULE=on
ENV GOFLAGS=-mod=vendor

FROM scratch
WORKDIR /app
COPY --from=0 /app/main .
CMD [ "/app/main" ]
